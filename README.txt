I worked alone on this program. I contributed all of the change since last week. To improve my
program, I first actually implemented minimax. I made a minimax function so that I could 
call it recursively. I also changed the way my score was calculated a little bit so instead of considering
the score after a specific move, it only considers the score based on the current state of the board.
The new code gives bonus points for controlling corners and takes away points for letting
the opponent control corners. I also used apha-beta pruning to allow my code to run faster. I
think my code will be good for the tournament because it thinks up to 5 turns in advance and
values corners.