#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <limits>
#include <algorithm>
using namespace std;

class Player {
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    vector<Move *> possible_moves(Side side, Board board);
    Board board;
    int Minimax(Board * temp, bool my_turn, int depth, int alpha, int beta);
    int Score(Board * temp);
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
	Side my_side;
	Side their_side;
	
};

#endif
