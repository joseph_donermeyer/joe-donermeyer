#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
     board = Board();
     my_side = side;
     
     
	 if(my_side == BLACK){
		 their_side = WHITE;
		 }
	else{
		their_side = BLACK;
	}
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
vector<Move *> Player::possible_moves(Side side, Board board){
	
	vector<Move *> moves;
	Move * move;
	//save all possible moves in an array
	for(int x = 0; x < 8; x++){
		for(int y = 0; y < 8; y++){
			move = new Move(x,y);
			if(board.checkMove(move, side)){
				moves.push_back(move);
				}
			}
		}
	return moves;
	}	
	
int Player::Minimax(Board * temp, bool my_turn, int depth, int alpha, int beta){
	int score;
	int copy_score;
	int a = alpha;
	int b = beta;
	
	//base case
	if(depth == 0) {
		score = Score(temp); 
	}
	else
	{
		if(my_turn){
			score = std::numeric_limits<int>::min();
			if(!temp->hasMoves(my_side)){
				score = Minimax(temp, !my_turn, depth - 1, a, b);
				}
				
			else{
				
				vector<Move *> possible = possible_moves(my_side, *temp);
				//runs minmax on each possible moves and chooses the max score.
				for(int i = 0; i < (int) possible.size(); i++){
					Board* copy = temp->copy();
					copy->doMove(possible[i], my_side);
						
					copy_score = Minimax(copy, !my_turn, depth - 1, a, b);
					if(copy_score > score){
						score = copy_score;
						}
					a = max(a, score);
						if( b < a){
							break;
						}	
					}	
				}
			}
			
		else{
			score =  std::numeric_limits<int>::max();
			if(!temp->hasMoves(their_side)){
				score = Minimax(temp, !my_turn, depth - 1, a, b);
				}
			else{
				vector<Move *> possible = possible_moves(their_side, *temp);
				//runs minmax on each possible move and considers  lowest min.
				for(int i = 0; i < (int) possible.size(); i++){
						Board* copy = temp->copy();
						copy->doMove(possible[i], their_side);
						double copy_score = Minimax(copy, !my_turn, depth - 1, a, b);
						
						if(copy_score < score){
							score = copy_score;
						}
						b = min(b, score);
						if( b < a){
							break;
						}	
					}
				}
			}
		}
	return score;
}

int Player::Score(Board * temp){
	int score = 10 * (temp->count(my_side) - temp->count(their_side));

	//checks the 4 corners
	if(temp->get(my_side, 0, 0)){
		
		score += 10000;
	}
	else if(temp->get(their_side, 0, 0)){
		score -= 10000;
	}
	if(temp->get(my_side, 0, 7)){
		score += 10000;
	}
	else if(temp->get(their_side, 0, 7)){
		score -= 10000;
	}
	
	if(temp->get(my_side, 7, 0)){
		score += 10000;
	}
	else if(temp->get(their_side, 7, 0)){
		score -= 10000;
	}
	if(temp->get(my_side, 7, 7)){
		score += 10000;
	}
	else if(temp->get(their_side, 7, 7)){
		score -= 10000;
	}
	return score; 
}	
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
     //place their move on the board
     board.doMove(opponentsMove, their_side);
     
     //check if moves are possible
     if(board.hasMoves(my_side)){
		 Move * move;
		 vector<Move *> possible = possible_moves(my_side, board);
		 	 
		 int alpha = std::numeric_limits<int>::min();
		 int beta = std::numeric_limits<int>::max();
		 
		 int score = std::numeric_limits<int>::min();
		 
		 
		//uses minimax on possible moves
			for(int i = 0; i < (int) possible.size(); i++){
				Board* copy = board.copy();
				copy->doMove(possible[i], my_side);
				int copy_score = Minimax(copy, false, 5, alpha, beta);
				
				if(copy_score > score){
					score = copy_score;
					move = possible[i];
					}
					
				alpha = max(alpha, score);
				
				}
		board.doMove(move, my_side);
		return move;
	}
    return NULL;
}
